# Table Tennis Booking App

The team enjoys playing table tennis. In the new office, there will be a table tennis, well, table! People can be playing any time of the day. But we only have one table, so players have to wait in line to play a game. 

This App enables used to book a slot in advance to optimize the process.

TechStack - AngularJS, MaterializeCSS and Firebase ( Auth, RealtimeDB & Hosting ).

# Local Installation and Execution

1. Install dependencies with npm

    	$ npm install

2. Run Application
		
        $ gulp watch

3. Open below link in browser

		http://localhost:8080

4. Test Application

    	$ karma start

5. Deploy on Firebase

    	$ firebase login
        $ firebase deploy
        

# Todo(s)

1. Create a Project Base

		Node, Express, Gulp, AngularJS, AngularFire, MaterializeCSS)
        
2. Add Test Runner & Write Test Scenarios

		Karma, Jasmine
        
3. Create a Firebase Project and Integrate Social Login using Firebse Auth
		
        Google
		Twitter
		Facebook

4. Key Features :

		4.1. Booking durations will need to be in minutes, ranging from 10 mins to 60 mins

		4.2. Anyone can book the table, but must have at least 60 min interval between each of their bookings

		4.3. No overlaps between bookings

		4.4. Each booking can be edited for only 2 minutes after they have been made but can be cancelled at anytime.
		
        4.5. Users can view their past bookings

5. Good To Have (Bonus Points):

		5.1. Users can login / logout to make their bookings
		
		5.2. Users can view their past bookings



# Demo

https://ttsbapp.firebaseapp.com

# Author

Nikhil Maheshwari (http://www.nikhilmaheshwari.com)