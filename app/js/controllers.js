angular.module('ttbApp.controllers', [])

.controller('LoginCtrl', function($scope, $rootScope, $firebaseAuth, $state) {
  console.log("in LoginCtrl  !");

  $scope.database = firebase.database();

  $scope.commonLogin = function(serviceProvider) {
    console.log('In commonLogin !');
    var provider, clientId, clientSecret;
    if (serviceProvider == 'google') {
      console.log('Processing google login !');
      provider = firebase.auth.GoogleAuthProvider;
      clientId = '744597744459-squdmsfk5g7tceeco8mtsoe1ju8nh0tm.apps.googleusercontent.com';
    } else if (serviceProvider == 'twitter') {
      console.log('Processing twitter login !');
    } else if (serviceProvider == 'facebook') {
      console.log('Processing facebook login !');
    }
    $scope.oauthLogin(provider, clientId, clientSecret);
  };

  $scope.oauthLogin = function(provider, clientId, clientSecret) {
    console.log('In oauthLogin !');
    $firebaseAuth(firebase.auth().signInWithPopup(new provider()).then(function(result) {
      console.log('result');
      console.log(result);
      $rootScope.userData = {
        email: result.user.email,
        displayName:result.user.displayName,
        photoURL: result.user.photoURL,
        uid: result.user.uid
      };
      $scope.updateUserToDB($rootScope.userData);
    }).catch(function(error) {
      alert(error)
      console.log(error);
    }));
  };

  $scope.updateUserToDB = function(user) {
    console.log('in updateUserToDB !');
    console.log(user);
    $scope.database.ref('users/' + user.uid).set(user);
    $state.go('booking');
  };

})

.controller('BookingCtrl', function($scope, $rootScope) {
  console.log("in BookingCtrl !");
  $scope.database = firebase.database();

  $scope.currentTime = new Date();
  $scope.minDate = new Date($scope.currentTime.getTime()).toISOString();
  $scope.maxDate = (new Date($scope.currentTime.getTime() + ( 1000 * 60 * 60 *24 * 15 ))).toISOString();
  $scope.slots = [];

  $scope.slotInputs = {
    date: '',
    time: '',
    duration: '',
    durations: [10, 20, 30, 40, 50, 60]
  };

  $scope.fetchUserEmail = function() {
    var userEmail = '';
    Object.keys(localStorage).forEach(function(key){
      if (key.indexOf('firebase:authUser') == 0) {
        userEmail =JSON.parse(localStorage[key]).email;
      }
    });
    return userEmail;
  };

  $scope.fetchSlotsFromDB = function() {
    console.log('fetchSlotsFromDB called !');

    if($scope.slots.length != 0) return;

    var configsRef = $scope.database.ref('slots/');
    configsRef.on('value', function(snapshot) {
      $scope.safeApply(function(){
        var snapshotVal = snapshot.val();
        $scope.slots = snapshotVal ? Object.keys(snapshotVal).map(function (key) {
          return snapshotVal[key];
        }) : [];
        console.log($scope.slots);
      });
    });
  };

  $scope.filteredSlots = function() {
    var userEmail = $rootScope.userData ? $rootScope.userData.email : $scope.fetchUserEmail();
    return $scope.slots.filter(function (slot) {
      return userEmail == slot.who;
    });
  };

  $scope.checkEditable = function(when) {
    console.log('checkEditable called !');
    var currentTime = Date.now();
    var minDiff = moment.duration(currentTime - when).asMinutes();
    console.log('minDiff : ', minDiff);
    return minDiff < 2 ;
  };

  $scope.bookSlot = function() {
    console.log('bookSlot called : ', $scope.slotInputs)

    if ($scope.slotInputs.date == '' || $scope.slotInputs.time == '' || $scope.slotInputs.duration == '') {
      alert('Please provide all the required values to book a slot !');
      return
    }

    var currentTime = Date.now();
    var dateArr = $scope.slotInputs.date.split('/'); // DD/MM/YYYY
    var timeArr = $scope.slotInputs.time.split(':'); // HH:MM
    var dateStr = $scope.slotInputs.date + ' ' + $scope.slotInputs.time
    var month = parseInt(dateArr[1]) - 1;
    var duration = parseInt($scope.slotInputs.duration);
    var startTime = moment(dateStr,"DD/MM/YYYY HH:mmA").toDate().getTime();
    
    if (startTime < currentTime) {
      alert('Slot can not be booked in past time !');
      return
    }

    // This logic works
    // var lastBookingTime;
    // $scope.slots.forEach(function(slot){
    //   if(slot.who == $rootScope.userData.email) {
    //     lastBookingTime = moment(new Date(slot.endTime));
    //   }
    // });

    // if (lastBookingTime) {
    //   var minDiff = moment.duration(startTime - lastBookingTime).asMinutes();
    //   console.log('minDiff : ', minDiff);
    //   if (minDiff < 60) {
    //     alert('Two slot can not be booked within one hour !');
    //     return
    //   }
    // }

    //var startTime = new Date(dateArr[2], month, dateArr[0], timeArr[0], timeArr[1]).getTime();
    var endTime = moment(startTime).add(duration, 'm').toDate().getTime();
    var slot = {
      startTime: startTime,
      endTime: endTime,
      duration: duration,
      who: $rootScope.userData.email,
      when: Date.now()
    }

    if( $scope.isSlotAvailable(slot) ) {
      $scope.slotInputs = {
        date: '',
        time: '',
        duration: '',
        durations: [10, 20, 30, 40, 50, 60]
      };

      console.log('slot : ', slot);
      $scope.database.ref('slots/' + startTime + endTime).set(slot);
      alert('Slot booked successfully !');
    } else {
      alert('Slot not available or booking within and hour!');
    }

  };

  $scope.editSlot = function(index) {
    console.log('editSlot called !');
    var slot = $scope.slots[index];
    var slotDetails = $scope.getTime(slot.startTime) + ' - ' +  $scope.getTime(slot.endTime) + ' ( ' + slot.duration + ' Min )';
    var userChoice = confirm('Your current booking will be deleted. Are you sure you want to edit ' + slotDetails + " ?");
    if (userChoice == true) {
      if ($scope.checkEditable(slot.when)) {
        alert('Opening your slot in editable mode !')
        $scope.slotInputs = {
          date: moment(slot.startTime).format('DD/MM/YYYY'),
          time: moment(slot.startTime).format('hh:mmA'),
          duration: slot.duration,
          durations: [10, 20, 30, 40, 50, 60]
        };
        $scope.database.ref('slots/' + slot.startTime + slot.endTime).set({});
        console.log('Deleted slot for Editing : ', slotDetails);
      } else {
        alert('Oh No !, Your can not edit this booking now !');
      }
    }
  };

  $scope.deleteSlot = function(index) {
    console.log('deleteSlot called !');
    var slot = $scope.slots[index];
    var slotDetails = $scope.getTime(slot.startTime) + ' - ' +  $scope.getTime(slot.endTime) + ' ( ' + slot.duration + ' Min )';
    var userChoice = confirm('Are you sure you want to delete ' + slotDetails + " ?");
    if (userChoice == true) {
        $scope.database.ref('slots/' + slot.startTime + slot.endTime).set({});
        console.log('Deleted slot : ', slotDetails);
    }
  };

  $scope.isSlotAvailable = function(newSlot) {
    var available = true;
    $scope.slots.forEach(function(oldSlot) {
      var startsAndEndsTogether = (newSlot.startTime == oldSlot.startTime) && (newSlot.endTime  == oldSlot.endTime)
      var startsBeforeEndsBetween = (newSlot.startTime <= oldSlot.startTime) && (newSlot.endTime > oldSlot.startTime) && (newSlot.endTime <= oldSlot.endTime);
      var startBetweenEndsAfter = (newSlot.startTime >= oldSlot.startTime) && (newSlot.startTime < oldSlot.endTime) && (newSlot.endTime >= oldSlot.endTime);
      var startsAndEndsBetween = (newSlot.startTime >= oldSlot.startTime) && (newSlot.endTime <= oldSlot.endTime);
      var bookingWithinAndHour = ( Math.abs(moment.duration(newSlot.startTime - oldSlot.endTime).asMinutes()) < 60 ) || ( Math.abs(moment.duration(oldSlot.startTime - newSlot.endTime).asMinutes()) < 60 );

      if ( startsAndEndsTogether || startsBeforeEndsBetween ||  startBetweenEndsAfter || startsAndEndsBetween || bookingWithinAndHour) {
        available =  false;
        console.log('Mismatch - ', 'oldSlot : ', oldSlot, 'newSlot : ', newSlot)
      }
    });
    return available;
  };

  $scope.getTime = function(timestamp, fromNow) {
    console.log('safeApply called !');
    return fromNow ? moment(timestamp).fromNow() : moment(timestamp).format("DD MMM YYYY hh:mm A");
  };

  $scope.safeApply = function(fn) {
    console.log('safeApply called !');
    if (this.$root) {
      var phase = this.$root.$$phase;
      if (phase == '$apply' || phase == '$digest') {
          if (fn && (typeof (fn) === 'function')) {
            fn();
          }
      } else {
          this.$apply(fn);
      }
    }
  };
});
