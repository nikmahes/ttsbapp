var gulp = require('gulp');
var concat = require('gulp-concat');
var ngAnnotate = require('gulp-ng-annotate');
var plumber = require('gulp-plumber');
var rename = require('gulp-rename');
var minifyHtml = require('gulp-minify-html')
var ngTemplate = require('gulp-ng-template')
var cleanCSS = require('gulp-clean-css');
var watch = require('gulp-watch');
var del = require('del');

var child = require('child_process');
var fs = require('fs');

gulp.task('clean', function () {
  return del(['app/build']);
});

gulp.task('templates', ['clean'], function() {
  return gulp.src('app/templates/*.html')
    .pipe(minifyHtml({empty: true, quotes: true}))
    .pipe(ngTemplate({
		moduleName: 'ttbApp.templates',
		standalone: true,
		wrap: false,
		useStrict: false,
		filePath: 'templates.js'
    }))
    .pipe(gulp.dest('app/js/'));
});

gulp.task('app', ['templates'], function() {
    gulp.src(['app/index.html']).pipe(gulp.dest('app/build'));
    return gulp.src(['app/app.js', 'app/**/*.js'])
	    .pipe(plumber())
		.pipe(concat('app.js', {newLine: ';'}))
		.pipe(ngAnnotate({add: true}))
	    .pipe(plumber.stop())
        .pipe(gulp.dest('app/build/js/'));
});

gulp.task('vendor', ['app'], function() {
    return gulp.src(['node_modules/jquery/dist/jquery.js',
    				 'node_modules/moment/moment.js',
    				 'node_modules/angular/angular.js',
    				 'node_modules/materialize-css/dist/js/materialize.js',
    				 'node_modules/angular-materialize/src/angular-materialize.js',
             'app/js/materialize..clockpicker.js',
    				 'node_modules/firebase/firebase.js',
    				 'node_modules/angularfire/dist/angularfire.js',
    				 'node_modules/angular-ui-router/release/angular-ui-router.js'])
	  .pipe(plumber())
		.pipe(concat('vendor.js', {newLine: ';'}))
		.pipe(ngAnnotate({add: true}))
	    .pipe(plumber.stop())
        .pipe(gulp.dest('app/build/js/'));
});

gulp.task('fonts', ['vendor'], function() {
    return gulp.src(['node_modules/materialize-css/dist/fonts/roboto/*.*'])
    	.pipe(gulp.dest('app/build/fonts/roboto'));
});

gulp.task('faw', ['fonts'], function() {
    return gulp.src(['node_modules/font-awesome/fonts/*.*'])
    	.pipe(gulp.dest('app/build/fonts'));
});

gulp.task('css', ['faw'], function() {
    return gulp.src('app/css/*.css')
		.pipe(concat('app.css'))
    	.pipe(cleanCSS({compatibility: 'ie8'}))
    	.pipe(gulp.dest('app/build/css'));
});

gulp.task('cssv', ['css'], function() {
    return gulp.src(['node_modules/font-awesome/css/font-awesome.css',
    				         'node_modules/materialize-css/dist/css/materialize.css',
                     'app/css/materialize.clockpicker.css'])
		.pipe(concat('vendor.css'))
    	.pipe(cleanCSS({compatibility: 'ie8'}))
    	.pipe(gulp.dest('app/build/css'));
});

gulp.task('watch', ['cssv'], function() {
    watch(['app/templates/*.html',
           'app/css/*.css',
           'app/js/controllers.js',
           'app/app.js',
           'app/index.html'], function() {
        gulp.start('cssv');
    });
});

gulp.task('server', function() {
  var server = child.spawn('node', ['server.js']);
  var log = fs.createWriteStream('server.log', {flags: 'a'});
  server.stdout.pipe(log);
  server.stderr.pipe(log);
});

gulp.task('default', ['server', 'watch']);
